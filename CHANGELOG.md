# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

<!--- next entry here -->

## 0.1.2
2019-08-25

### Fixes

- testing hotfix flow (5bf03a0239ca2078ea26c380e98370446922d750)

## 0.1.1
2019-08-25

### Fixes

- README (cd4fd8e79cbbd2b04102b5618330e0b2a960cb1c)

## 0.1.0
2019-08-25

### Features

- Initial Commit (edfdc822d1dd727885b4dcd61ac7ad6288f9bf63)

